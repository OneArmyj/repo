public class Circle {
    private Point point1;
    private Point point2;
    private Point midpointLine;
    private double angle;
    private Point CentreOfCircle;
    
    public Circle() {};

    public Circle(Point p, Point q) {
        this.point1 = p;
        this.point2 = q; 
        this.midpointLine = p.makeMid(q);
        this.angle = p.angleTo(q);
    }

    public void printCircle() {
        System.out.printf("%s and %s coincides with circle of centre %s\n", this.point1, this.point2, this.CentreOfCircle);
    }

    public void makeCentre() {
        double pm = this.point1.getDistance(this.midpointLine);
        double cm = Math.sqrt(1 - Math.pow(pm, 2));
        double tanAngle = this.angle + (Math.PI / 2);
        double xmove = Math.cos(tanAngle) * cm;
        double ymove = Math.sin(tanAngle) * cm;
        this.CentreOfCircle = new Point(this.midpointLine.getX() + xmove, this.midpointLine.getY() + ymove);
    }

    public int getCoverage(Point[] ArrP) {
        int count = 0;
        for (int i = 0; i < ArrP.length; i++) {
            if (this.CentreOfCircle.getDistance(ArrP[i]) <= 1) {
                count += 1;
            }
        }
        return count;
    }
}

